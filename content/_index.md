---
title: "Live Coding: A User's Manual"
date: 2022-09-24
toc: false
image:
  filename: covers/lcaum.png
  caption: hmm
  placement: 2
  focal_point: "Left"
---

Performative, improvised, on the fly: live coding is about how people
interact with the world and each other via code. In the last few
decades, live coding has emerged as a dynamic creative practice,
gaining attention across cultural and technical fields—from music and
the visual arts to computer science. 

Live Coding: A User's Manual is the first comprehensive introduction
to the practice and a broader cultural commentary on the potential for
live coding to open up deeper questions about contemporary cultural
production and computational culture. This multiauthored book—by
artists and musicians, software designers, and researchers—provides a
practice-focused account of the origins, aspirations, and evolution of
live coding, including expositions from a wide range of live coding
practitioners. In a more conceptual register, the authors consider
liveness, temporality, and knowledge in relation to live coding,
alongside speculating on the practice's future forms.

## Read the book!

This book is published open access by MIT Press, and will be freely
available for download from this website in PDF, Mobi and Epub form,
from its formal publication date on 22nd November 2022.

Live Coding is also widely available in paperback. Please consider using an [ethical
bookseller](https://www.ethicalbooksearch.com/books/m/is:9780262372626/live-coding-a-user-s-manual-software-studies-alan-f-blackwell-emma-cocker-geoff-cox-alex-mclean-thor-magnusson).

## Authors

This book is written by Alan Blackwell, Emma Cocker, Geoff Cox, Alex
McLean and Thor Magnusson, with the exception of the expositions
chapter, which consists of contributions from Rangga Aji, ALGOBABEZ,
Jack Armitage, Rafaele Andrade, Pietro Bapthysthe, Lina Bautista,
Renick Bell, Alexandra Cardenas, Lucy Cheesman, Joana Chicau, Nick
Collins, Malitzin Cortes, Mamady Diara, Claudio Donaggio, Rebecca, a
Fernandes, Jason Freeman, Flor de Fuego, Sarah Groff Hennigh-Palermo,
Mike Hodnick, Timo Hoogland, Miri Kaat, Abhinay Khoparzi, Shawn
Lawson, Melody Loveless, Mynah Marie, Fabrice Mogini, Kofi Oduro,
David Ogborn, Jonathan Reus, MicoRex, Antonio Roberts, Charlie
Roberts, Jessica Rodriguez, Iris Saladino, Kate Sicchio, th4, Rodrigo
Velasco, Elizabeth Wilson, and Anna Xambo.

Please refer to the acknowledgments section of the book for a
(unfortunately but necessarily incomplete) list of all those who made
this book possible.

We hope that as an open access text, that the list of authors grows as
future contributors take it further.

## Design

The cover design is by [Joana Chicau](https://joanachicau.com/), using FT88 from the [Degheest
font family](https://velvetyne.fr/fonts/degheest/) designed by Mandy
Elbé and Oriane Charvieux, based on typeface by Ange Degheest.
